var config = {
  apiKey: "",
  authDomain: "dino-ee4c1.firebaseapp.com",
  databaseURL: "https://dino-ee4c1.firebaseio.com",
  projectId: "dino-ee4c1",
  storageBucket: "dino-ee4c1.appspot.com",
  messagingSenderId: ""
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.usePublicVapidKey("");

messaging.requestPermission().then(function() {
  console.log('Notification permission granted.');
}).catch(function(err) {
  console.log('Unable to get permission to notify.', err);
});

messaging.getToken().then(function(currentToken) {
  if (currentToken) {
    console.log('Current token.');
    console.log(currentToken);
  } else {
    console.log('No Instance ID token available. Request permission to generate one.');
  }
}).catch(function(err) {
  console.log('An error occurred while retrieving token. ', err);
});

messaging.onTokenRefresh(function() {
  messaging.getToken().then(function(refreshedToken) {
    console.log('Token refreshed.');
    console.log(refreshedToken);
  }).catch(function(err) {
    console.log('Unable to retrieve refreshed token ', err);
  });
});

messaging.onMessage(function(payload) {
  console.log('Message: ', payload);
});
