
importScripts('https://www.gstatic.com/firebasejs/5.1.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.1.0/firebase-messaging.js');

var config = {
  apiKey: "",
  authDomain: "dino-ee4c1.firebaseapp.com",
  databaseURL: "https://dino-ee4c1.firebaseio.com",
  projectId: "dino-ee4c1",
  storageBucket: "dino-ee4c1.appspot.com",
  messagingSenderId: ""
};

firebase.initializeApp(config);

const messaging = firebase.messaging();
