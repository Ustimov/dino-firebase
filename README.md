# Getting started

### App URL

`https://dino-ee4c1.firebaseapp.com/`

### Request URL

`https://fcm.googleapis.com/fcm/send`

### Request body

```json
{
	"to": "eiVlaRxB5-8:APA91bFIXG2a-abwXnwQB1M_ATHAN91xy5ld2Yjdgi1dV-f85jxPiwrZLF91MjRW32eNG9iiEBa2LrCuewsQj3Y_y2QHai_7aKnY6NSXYLjZf7H8xfwmNou6g8uJso0H2IflkmkVW0zi",
	"notification": {
		"title": "DiNo",
		"body": "DiNo",
		"click_action" : "https://dino-ee4c1.firebaseapp.com/"
	}
}
```